from numpy import transpose
import time, random
from collections import Counter

class Dense_Unit:
    # class-data structure for the primitive unit that will be used later everywhere
    def __init__(self, units, points = set()):
        # self.axes = axes # TBD - to delete???
        self.units = units # units - list of Primitive_Units
        # find points in the dense unit (for simplification uses just custom points)
        self.points = points if points else set.intersection( *( u.points for u in units ) )

    def merge(self, dense_unit2, density_th, point_weights = [], simplified = False):
        '''
        Merge two dense units. Returns None in case of unacceptance / error
        ''' 
        if not simplified and set( self.units[:-1] ) != set( dense_unit2.units[:-1] ):
            return None
        prob_points = self.points & dense_unit2.points
        # define density
        density = sum([point_weights[p] for p in prob_points]) if point_weights else len(prob_points)
        if density >= density_th:
            units = sorted(set(self.units) | set(dense_unit2.units), key = lambda x : x.axis)
            return Dense_Unit( units = units, points = prob_points )
        return None

    def __str__(self):
        return "%s , %s" % (self.units, len(self.points))

class Primitive_Unit:
    # class-data structure for the primitive unit that will be used later everywhere
    def __init__(self, iid, begin, end, does_include, axis, points = set()):
        self.iid = iid
        self.axis = axis
        self.begin = begin
        self.end = end
        self.does_include = does_include
        self.points = points

    def is_dense(self, density_th, point_weights = []):
        if point_weights:
            # unit_density = sum([point_weights.get(point, 1) for point in self.points])
            unit_density = sum([point_weights[point] for point in self.points])
        else:
            unit_density = len( self.points )
        return unit_density >= density_th

    def __str__(self):
        close_bracket = "]" if self.does_include else ")"
        return "axis %s [%s,%s%s; %s points" % (self.axis, self.begin, self.end, close_bracket, len(self.points))

class Subspace:
    def __init__(self, subspace1, subspace2, density_th, point_weights = [], simplified = False):
        self.axes = sorted( set(subspace1.axes) | set(subspace2.axes) )
        self.dense_areas = self.set_dense_areas( subspace1.get_dense_areas(), subspace2.get_dense_areas(), density_th, point_weights, simplified = simplified)

    def set_dense_areas(self, dense_areas1, dense_areas2, density_th, point_weights = [], simplified = False):
        # for all possible combinations of units - find dense areas with 
        dense_areas = []
        for da1 in dense_areas1:
            for da2 in dense_areas2:
                dense_unit = da1.merge(da2, density_th, point_weights, simplified = simplified)
                if dense_unit:
                    dense_areas.append( dense_unit )
        return dense_areas

    def get_dense_areas(self):
        return self.dense_areas

    def is_mergeable(self, subspace2, simplified = False):
        if simplified: 
            return not set( self.axes ) & set( subspace2.axes )
        return set( self.axes[:-1] ) == set( subspace2.axes[:-1] )

    def merge(self, subspace2, density_th, point_weights = [], simplified = False):
        if not self.is_mergeable(subspace2, simplified = simplified):
            return None
        if simplified or self.axes[-1] < subspace2.axes[-1]: # be sure about the order
            return Subspace(self, subspace2, density_th, point_weights, simplified = simplified)
        return Subspace(subspace2, self, density_th, point_weights)

    def is_valid(self):
        '''
        contains at least one dense area
        '''
        return len(self.dense_areas) > 0

    def __str__(self):
        return "axes %s dense_areas : %s" % (self.axes, ','.join([str(d) for d in self.dense_areas]) )

class Primitive_Subspace(Subspace):
    def __init__(self, id, coordinates, partitions_number, density_th, point_weights = [], points = None, custom_min = None, custom_max = None):
        '''
        Subspace() initiates the primitive subspace - 1 dimensional subspace, partitions it and finds dense cells
        '''
        self.iid = id
        self.axes = [id,]
        self.primitive_units = self.set_primitive_units(id, coordinates, partitions_number, density_th, point_weights = point_weights, points = points, custom_min = custom_min, custom_max = custom_max)
        self.dense_areas = self.set_dense_areas()

    def set_primitive_units(self, subspace_id, coordinates, partitions_number, density_th, point_weights = [], points = None, custom_min = None, custom_max = None):
        mmax = custom_max if custom_max is not None else max(coordinates)
        mmin = custom_min if custom_min is not None else min(coordinates)
        # include the last point - @TBD
        # mmax += ( mmax - mmin ) / partitions_number / 100 # just to be sure
        partition_size = ( mmax - mmin ) / partitions_number
        partitions = self.get_partitions(subspace_id, mmin, mmax, partitions_number)
        # mapp points with coordinate.
        # use abstract enumerate by default and specific points if provided
        point_coord_list = zip(points, coordinates) if points else enumerate(coordinates)

        temp_dist_parition = [ [] for _ in range(partitions_number) ] # to avoid too much interesections
        for point, coord in point_coord_list:
            partition_id = min(int( (coord - mmin) / partition_size ), partitions_number - 1)
            temp_dist_parition[ partition_id ].append( point )

        for partition_id in range(partitions_number):
            partitions[ partition_id ].points = set( temp_dist_parition[ partition_id ] )
        # filter only partitions - which are dense.
        return [_part for _part in partitions if _part.is_dense(density_th, point_weights)]

    def get_partitions(self, subspace_id, mmin, mmax, partitions_number):
        partition_size = ( mmax - mmin ) / partitions_number
        return [
            Primitive_Unit(
                iid = i,
                begin = mmin + i * partition_size,
                end = mmin + (i + 1) * partition_size,
                does_include = i == partitions_number - 1,
                axis = subspace_id
                )
            for i in range(partitions_number)
            ] 

    def get_primitive_units(self):
        return self.primitive_units

    def get_primitive_units_dict(self):
        return { prim.iid : prim for prim in self.primitive_units}

    def set_dense_areas(self):
        temp_prims = self.primitive_units
        return [
            Dense_Unit(units = [prim,], points = prim.points ) 
                for prim in temp_prims
            ]

class Clique:
    def __init__(self, data, partitions_number, density_th, simplified = True, point_weights = [], kvargs = {}):
        '''
        data parameter is point oriented. Ie., data = [[x1,y1,z1],[x2,y2,z2]]
        kwargs = {
            "custom_mins" : [<-dimensionality->],
            "custom_maxs" : [<-dimensionality->],
            }

        @TODO: point_weights - think about moving to list of weights. Don't deal with ids!
        '''
        self.point_number = len(data)
        if point_weights and len(point_weights) != self.point_number:
            raise ValueError("point_weights should be the same size as data")
        data_T = transpose(data)
        if simplified:
            self.init_simplified(data_T, partitions_number, density_th, point_weights, kvargs)
        else:
            self.init_straight(data_T, partitions_number, density_th, point_weights, kvargs)
        self.find_clusters_from_dense_areas()

    def get_clusters_vector(self):
        '''
        returns final vector of clusters
        '''
        return self.final_cluster_points

    def merge_process(self, subspaces, density_th, point_weights = []):
        '''
        Used for **simplified** situation
        '''
        if not subspaces:
            print("Merge process requires some subspacess")
            return None
        if len(subspaces) == 1:
            return subspaces, []
        not_connected_subspaces = []
        good_subspaces = [s for s in subspaces if s.is_valid()] # check just in case
        product_subspaces = []

        product_subspaces.append(good_subspaces[0])
        dimensionality = len(good_subspaces)
        for i in range(1,dimensionality):
            temp_product_subspace = product_subspaces[-1].merge(good_subspaces[i], density_th, point_weights, simplified = True)
            if temp_product_subspace and temp_product_subspace.is_valid():
                product_subspaces.append(good_subspaces[i])
                product_subspaces.append(temp_product_subspace)
            else:
                not_connected_subspaces.append(good_subspaces[i])
        # returns   1) resulted product subspaces in case if the result is valid
        #           2) not connected subspaces (which have non-valid intersection)
        return product_subspaces, not_connected_subspaces

    def init_simplified(self, data, partitions_number, density_th, point_weights = [], kvargs = {}):
        dimensionality = len(data)
        custom_mins, custom_maxs = [None for _ in range(dimensionality)], [None for _ in range(dimensionality)]
        if "custom_mins" in kvargs:
            custom_mins = kvargs["custom_mins"]
        if "custom_maxs" in kvargs:
            custom_maxs = kvargs["custom_maxs"]

        primitive_subspaces = [
            Primitive_Subspace(
                id = id,
                coordinates = coordinates,
                partitions_number = partitions_number,
                density_th = density_th,
                custom_min = custom_mins[id],
                custom_max = custom_maxs[id],
                point_weights = point_weights
            ) for id, coordinates in enumerate(data)
        ]
        
        self.non_valid_primitive_subspaces = [p for p in primitive_subspaces if not p.is_valid()]

        non_connected_subspaces = [p for p in primitive_subspaces if p.is_valid()]
        product_subspaces_trials = []
        dims = len(non_connected_subspaces)
        product_subspaces = [] # [0 for i in range(dims)]
        for i in range(dims): # can do while(non_connected_subspaces):
            temp_product_subspace, non_connected_subspaces = self.merge_process(non_connected_subspaces, density_th, point_weights)
            product_subspaces.append(temp_product_subspace)
            if not non_connected_subspaces:
                break
        self.product_subspaces = product_subspaces
        self.last_step_subspace = []

    def init_straight(self, data, partitions_number, density_th, point_weights = [], kvargs = {}):
        dimensionality = len(data)
        custom_mins, custom_maxs = [None for _ in range(dimensionality)], [None for _ in range(dimensionality)]
        if "custom_mins" in kvargs:
            custom_mins = kvargs["custom_mins"]
        if "custom_maxs" in kvargs:
            custom_maxs = kvargs["custom_maxs"]

        previous_subspaces = [
            Primitive_Subspace(
                id = id,
                coordinates = coordinates,
                partitions_number = partitions_number,
                density_th = density_th,
                custom_min = custom_mins[id],
                custom_max = custom_maxs[id],
                point_weights = point_weights
            ) for id, coordinates in enumerate(data)
        ]
        bad_subspaces = [] # if there is nothing
        current_subspaces = []
        for permut in range(1,dimensionality): # already did first round with primitives
            for i, sp1 in enumerate(previous_subspaces):
                for sp2 in previous_subspaces[i+1:]:
                    temp_sp = sp1.merge(sp2, density_th, point_weights)
                    if temp_sp and temp_sp.is_valid():
                        current_subspaces.append(temp_sp)
                    else:
                        bad_subspaces.append(temp_sp)
            previous_subspaces = current_subspaces
            current_subspaces = []

        self.product_subspaces = []
        self.last_step_subspaces = previous_subspaces
            
    def find_clusters_from_dense_areas(self):
        # adapted only for simplified version now
        if self.product_subspaces:
            biggest_product_list = max(self.product_subspaces, key = lambda x : len(x) )
        elif self.last_step_subspaces:
            biggest_product_list = self.last_step_subspaces
        biggest_product_subspace = biggest_product_list[-1]
        print("INFO: Find clusters in the subspace of %s axes.\nIn total: %s dense_unit(s)." % (biggest_product_subspace.axes, len(biggest_product_subspace.dense_areas) ) )
        dense_areas = biggest_product_subspace.dense_areas
        clusters_list = list(range(len(dense_areas)))
        for i, da1 in enumerate(dense_areas):
            for j, da2 in enumerate(dense_areas):
                if i >= j:
                    continue
                intersection = set(da1.units) & set(da2.units)
                dif = list((set(da1.units) | set(da2.units) ) - intersection)
                if len(dif) == 2 and abs(dif[0].iid - dif[1].iid) == 1:
                    if j == clusters_list[j]: # it's never been clustered before
                        clusters_list[j] = clusters_list[i]
                    elif j != clusters_list[j]: # it's been clustered before by more major (earlier) dense area
                        clusters_list = [c if c != clusters_list[i] else clusters_list[j] for c in clusters_list]

        self.final_cluster_points = [-1 for p in range(self.point_number)]
        for i, c in enumerate(clusters_list):
            for point in dense_areas[i].points:
                self.final_cluster_points[point] = c
        print("Here should be cluster description.")
